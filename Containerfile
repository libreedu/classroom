# SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
#
# SPDX-License-Identifier: AGPL-3.0-or-later

FROM --platform=$BUILDPLATFORM docker.io/golang:bullseye AS build

COPY . /src

WORKDIR /src
# RUN go mod download
RUN CGO_ENABLED=1 go build -o /src/plugin-example.so -trimpath -buildmode=plugin ./plugins/example
RUN CGO_ENABLED=1 go build -o /src/server -trimpath -buildmode=pie ./cmd/classroom

# Debian Bookworm has glibc 2.34 (2.32 or 2.34 is required for cgo),
# even though it builds/links on Bullseye which has 2.31. What??
FROM docker.io/debian:bookworm-slim AS bin

WORKDIR /app
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /src/plugin-example.so /app/plugins/example/plugin.so
COPY --from=build /src/server ./classroom

EXPOSE 3000
CMD ["/app/classroom"]
