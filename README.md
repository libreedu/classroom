# Dachen Classroom

[![Status: In Development](./docs/badges/status.svg)](#)
[![Version: —](./docs/badges/version.svg)](https://codeberg.org/libreedu/classroom/releases)
[![License: AGPLv3](./docs/badges/license.svg)](#)
[![Issues](./docs/badges/issues.svg)](https://codeberg.org/libreedu/classroom/issues)
[![Issues](./docs/badges/website.svg)](https://dachenedu.org/software/classroom/)

Dachen Classroom is a modern student information system and learning
management system, all in one.

## Why Dachen Classroom?

The idea of Dachen Classroom is that, since your student information
system and learning management system both process much of the same
information, they should be one in the same. Dachen Classroom aims to be
easily interoperable with any other software used in your school for
collaboration.

## Running

Copy config.example.yml to config.yml. If running with Docker Compose,
change `localhost` to `db`.

```
$ podman-compose build
$ podman-compose up
```

For development, comment out `app` and use the Justfile recipe `dev`.

```
$ podman-compose build
$ podman-compose up
$ just dev
```

## Planned features

- OneRoster compliance

## Notes

This can be built with `CGO_ENABLED=0` if plugins are disabled. Use the
build flag `-tags noplugins`.
