// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package db

import (
	"context"
	"time"

	"github.com/uptrace/bun"
)

type User struct {
	bun.BaseModel `bun:"table:users"`
	Subject       string `json:"-" bun:"sub"`
	Issuer        string `json:"-" bun:"iss"`
	ID            string `json:"id" bun:",pk,type:uuid"`

	GivenName  string `json:"given_name"`
	FamilyName string `json:"family_name"`
	MiddleName string `json:"middle_name"`
	NameSuffix string `json:"name_suffix"`
	Email      string `json:"email"`

	School     string                 `json:"school" bun:",type:uuid"`
	CustomData map[string]interface{} `json:"custom_data"`

	UserPreferences map[string]interface{} `json:"user_preferences"`
	LastLogin       time.Time              `json:"last_login"`

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func GetUser(id string) (User, error) {
	ctx := context.Background()
	m := new(User)
	err := DB.NewSelect().Model((*User)(nil)).Where("id = ?", id).Scan(ctx, m)
	return *m, err
}
