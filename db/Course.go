// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package db

import (
	"context"
	"time"

	"github.com/uptrace/bun"
)

type Course struct {
	bun.BaseModel `bun:"table:courses"`
	ID            string `json:"id" bun:",pk,type:uuid"`
	Subject       string `json:"subject" bun:",type:uuid"`
	GradeLevel    int16  `json:"grade_level"`
	Title         string `json:"title"`
	ShortName     string `json:"short_name"`
	Description   string `json:"description"`

	School     string `json:"school" bun:",type:uuid"`
	SchoolYear string `json:"school_year"`

	CreditHours float32 `json:"credit_hours"`

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func GetCourse(id string) (Course, error) {
	ctx := context.Background()
	m := new(Course)
	err := DB.NewSelect().Model((*Course)(nil)).Where("id = ?", id).Scan(ctx, m)
	return *m, err
}
