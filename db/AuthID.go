package db

import (
	"context"
	"time"

	"github.com/uptrace/bun"
)

type AuthID struct {
	bun.BaseModel `bun:"table:auth_tokens"`
	ID            string
	UID           string `bun:"uid"`
	IssuedAt      time.Time
}

func GetAuthID(token string) (AuthID, error) {
	ctx := context.Background()
	var t = new(AuthID)
	err := DB.NewSelect().Model((*AuthID)(nil)).Where("id = ?", token).Scan(ctx, t)
	return *t, err
}

func CheckAuthID(authId string) (User, AuthID, bool) {
	v, err := GetAuthID(authId)
	if v.IssuedAt.Add(7 * 24 * time.Hour).Before(time.Now()) {
		return User{}, v, false
	}
	u, erru := GetUser(v.UID)
	if erru != nil {
		panic("token user does not exist")
	}
	return u, v, err == nil
}
