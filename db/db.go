// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package db

import (
	"context"
	"fmt"
	"time"

	"codeberg.org/libreedu/common/log"
	"github.com/spf13/viper"

	"database/sql"

	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
	"github.com/uptrace/bun/driver/pgdriver"
)

var DB *bun.DB

// Function returns when the database connection is established or fails
func InitHeadless() error {
	dsn := viper.GetString("DB_ADDR")
	pgdb := sql.OpenDB(pgdriver.NewConnector(pgdriver.WithDSN(dsn)))
	DB = bun.NewDB(pgdb, pgdialect.New())

	ctx := context.Background()
	for {
		var n int
		if err := DB.NewSelect().ColumnExpr("15").Scan(ctx, &n); err != nil {
			continue
		}
		if n != 15 {
			// FIXME: This currently is unconsequential, but the library
			// should provide a logger.Panicf like the standard library `log`.
			return fmt.Errorf("n != 15 from database")
		}
		return nil
	}
}

func Init(logger *log.Logger) {
	dsn := viper.GetString("DB_ADDR")
	pgdb := sql.OpenDB(pgdriver.NewConnector(pgdriver.WithDSN(dsn)))
	DB = bun.NewDB(pgdb, pgdialect.New())

	var s time.Duration = 1 * time.Second
	ticker := time.NewTicker(s)
	ctx := context.Background()
	for range ticker.C {
		var n int
		if err := DB.NewSelect().ColumnExpr("15").Scan(ctx, &n); err != nil {
			logger.Warnf("PostgreSQL error: %s", err.Error())
			logger.Warnf("Retrying in %d seconds", s/time.Second)
			ticker.Reset(s)
			s *= 2
			continue
		}
		if n != 15 {
			// FIXME: This currently is unconsequential, but the library
			// should provide a logger.Panicf like the standard library `log`.
			logger.Fatalf("n != 15 from database")
		}
		break
	}
}
