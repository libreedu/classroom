// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package db

import (
	"context"
	"time"

	"github.com/uptrace/bun"
)

type School struct {
	bun.BaseModel `bun:"table:schools"`
	ID            string `json:"id" bun:",pk,type:uuid"`
	SchoolYear    int16  `json:"school_year"`

	Title                string  `json:"title"`
	ShortName            string  `json:"short_name"`
	Address              address `json:"address"`
	Principal            string  `json:"principal" bun:",type:uuid"`
	WebAddress           string  `json:"web_address"`
	SchoolNumber         string  `json:"school_number"`
	ScheduleRotationDays int     `json:"schedule_rotation_days"`

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func GetSchool(id string) (School, error) {
	ctx := context.Background()
	m := new(School)
	err := DB.NewSelect().Model((*School)(nil)).Where("id = ?", id).Scan(ctx, m)
	return *m, err
}
