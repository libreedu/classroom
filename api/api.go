// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package api

import (
	"codeberg.org/libreedu/common/log"
	"github.com/go-chi/chi/v5"
)

var logger *log.Logger

func Init(pl *log.Logger) func(r chi.Router) {
	logger = pl

	return func(r chi.Router) {
		r.Group(loginEndpoints)
	}
}
