// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package api

import (
	"context"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"codeberg.org/libreedu/classroom/db"
	"codeberg.org/libreedu/common/util"
	"github.com/coreos/go-oidc/v3/oidc"
	"github.com/go-chi/chi/v5"
	json "github.com/json-iterator/go"
	"github.com/patrickmn/go-cache"
	"github.com/spf13/viper"
)

// TODO: store in database
var providers = cache.New(8*time.Hour, 4*time.Hour)

// TODO: support registration
func loginEndpoints(r chi.Router) {
	r.Get("/api/v0/user/login", func(w http.ResponseWriter, r *http.Request) {
		var sub, iss string
		{
			acct := strings.Split(r.URL.Query().Get("id"), "@")
			if len(acct) < 2 {
				util.JSON(w, map[string]string{"error": "invalid id"}, 400)
				return
			}
			iss = acct[len(acct)-1]
			sub = strings.Join(acct[0:len(acct)-1], "@")
		}
		sub += "" // FIXME

		oidcServer := ""
		wf, err := http.Get((&url.URL{
			Scheme: "https",
			Host:   iss,
			Path:   ".well-known/webfinger",
			RawQuery: (url.Values{
				"resource": {r.URL.Query().Get("id")},
				"rel":      {"http://openid.net/specs/connect/1.0/issuer"},
			}.Encode()),
		}).String())
		if err != nil || wf.StatusCode-200 > 100 {
			util.JSON(w, map[string]string{"error": "webfinger error"}, 500)
			return
		}
		if bytes, err := io.ReadAll(wf.Body); err != nil {
			util.JSON(w, map[string]string{"error": "webfinger error"}, 500)
			return
		} else {
			links := new(struct {
				Links []struct {
					Rel  string
					Href string
				}
			})
			if json.Unmarshal(bytes, &links) != nil {
				util.JSON(w, map[string]string{"error": "webfinger error"}, 500)
				return
			}
			for _, v := range links.Links {
				if v.Rel == "http://openid.net/specs/connect/1.0/issuer" {
					oidcServer = v.Href
					break
				}
			}
			if oidcServer == "" {
				util.JSON(w, map[string]string{"error": "webfinger error"}, 500)
				return
			}
		}

		var p *oidc.Provider
		if P, ok := providers.Get(oidcServer); ok {
			p = P.(*oidc.Provider)
		} else {
			P, err := oidc.NewProvider(context.Background(), oidcServer)
			if err != nil {
				logger.Error() <- "Discovery error: " + err.Error()
				util.JSON(w, map[string]string{"error": "discovery error"}, 500)
				return
			}
			providers.Set(oidcServer, P, 0)
			p = P
		}

		endpoint, err := url.Parse(p.Endpoint().AuthURL)
		if err != nil {
			util.JSON(w, map[string]string{"error": "discovery error"}, 500)
			return
		}
		eq := endpoint.Query()
		eq.Set("response_mode", "query")
		eq.Set("response_type", "id_token")
		eq.Set("scope", url.QueryEscape("openid profile email"))
		eq.Set("redirect_uri", (&url.URL{Scheme: "https", Host: viper.GetString("DOMAIN"), Path: "/api/user/login/callback", RawQuery: "s=oidc1"}).String())
		if endpoint.Scheme == "openid" {
			eq.Set("client_id", eq.Get("redirect_uri"))
			endpoint.Opaque = "//"
		}
		endpoint.RawQuery = eq.Encode()

		w.Header().Add("Location", endpoint.String())
		w.WriteHeader(302)
	})

	r.Get("/api/v0/user/login/callback", func(w http.ResponseWriter, r *http.Request) {})

	r.Get("/api/v0/user/login/valid/{authId}", func(w http.ResponseWriter, r *http.Request) {
		_, _, valid := db.CheckAuthID(chi.URLParam(r, "authId"))
		util.JSON(w, map[string]interface{}{"valid": valid}, 0)
	})
}
