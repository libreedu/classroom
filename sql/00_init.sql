-- SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

create table schools (
  id uuid unique not null,
  school_year numeric(4, 0) not null,
  primary key (id, school_year),

  title varchar(100) not null,
  short_name varchar(25) not null,
  address json, -- OpenID Connect 1.0 `address` claim
  principal uuid not null, -- No foreign key, enforce application-side
  web_address text,
  school_number varchar(25),
  schedule_rotation_days integer default 1,

  created_at timestamp default current_timestamp,
  updated_at timestamp
);

create table users (
  sub varchar(256), -- For OpenID authentication
  iss varchar(256),
  id uuid primary key,

  given_name  varchar(50) not null,
  family_name varchar(50) not null,
  middle_name varchar(50),
  name_suffix varchar(3),
  email text, -- TODO: there should be further validation, either
              -- application-side or in the SQL.

  school uuid not null references schools(id),
  custom_data json not null default '{}'::json,

  user_preferences json not null default '{}'::json,
  last_login timestamp,

  created_at timestamp default current_timestamp,
  updated_at timestamp
);

create table school_marking_periods (
  id uuid primary key,

  school_year numeric(4, 0) not null,
  school uuid,
  foreign key (school, school_year) references schools(id, school_year),

  mp varchar(3) not null,
  title varchar(50) not null,
  short_name varchar(10),
  sort_order numeric, -- Higher values sort later
  start_date date not null,
  end_date   date not null,
  post_start_date date, -- The earliest and—
  post_end_date   date, -- latest dates at which a teacher can input
                        -- grades. By default, equivalent to the normal
                        -- start_date and end_date values.
  does_grades   boolean, -- Whether teachers can input grades and—
  does_comments boolean, -- pupil comments during the marking period.

  created_at timestamp default current_timestamp,
  updated_at timestamp
);

create table courses (
  id uuid primary key,
  subject uuid not null,
  grade_level integer,
  title varchar(100) not null,
  short_name varchar(25),
  description text,

  school uuid not null,
  school_year numeric(4, 0),
  foreign key (school, school_year) references schools(id, school_year),

  credit_hours numeric(6, 2),

  created_at timestamp default current_timestamp,
  updated_at timestamp
);

create table course_periods (
  id uuid primary key,

  school uuid not null,
  school_year numeric(4, 0) not null,
  foreign key (school, school_year) references schools(id, school_year),

  teacher           uuid references users(id),
  secondary_teacher uuid references users(id),
  title text,
  room varchar(10),
  course uuid references courses(id),
  marking_period uuid references school_marking_periods(id),
  total_seats  integer,
  filled_seats integer,

  does_attendance text, -- ????
  does_honor_roll boolean,
  does_class_rank boolean,

  created_at timestamp default current_timestamp,
  updated_at timestamp
);

create table auth_tokens (
  id uuid unique primary key not null,
  uid uuid not null references users(id),
  issued_at timestamp not null
);
