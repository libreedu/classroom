# SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
#
# SPDX-License-Identifier: AGPL-3.0-or-later

build:
  @# CGO makes the program dynamically linked, allowing plugin support.
  CGO_ENABLED=1 go build -trimpath -buildmode=pie ./cmd/classroom

clean:
  -rm classroom

prettify:
  go fmt

plugin name:
  cd './plugins/{{name}}' && go build -o plugin.so -trimpath -buildmode=plugin .

dev: build
  podman run -i --network="$(basename `pwd`)_default" --name classroom -p 3000:3000 -v .:/app -w /app docker.io/debian:bookworm /app/classroom

sdev:
  -podman rm classroom
