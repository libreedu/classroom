// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { createElement, createFragment, Props } from '../jsx';

interface IClassroom {
  /** The UUID identifying the class. */
  id: string;
  /** The name of the class. */
  name: string;
  subtitle: string;
  /** Map of other properties. */
  metadata: Record<string, any> & {
    room?: string;
    course_name?: string;
    /** The splash image for the class. */
    splash?: {
      color: string;
      image: string;
    };
  };
  /** The list of users who are teachers in the class. */
  teachers: string[];
  /**
   * Weighting categories. Total weight is simply the sum of all
   * categories, and grades are weighted proportionally.
   */
  weighting_categories: Record<
    string,
    {
      name: string;
      /**
       * Floating-point number describing the relevant proportion of a
       * class grade.
       */
      weight: number;
    }
  >;
  archived: boolean;
}

export default function Classroom(props: Props, ...children): Node {
  document
    .getElementById('top-bar-title')
    .replaceChildren(
      <h1 className="class-name">{props.classroom.name}</h1>,
      props.classroom.subtitle ? (
        <span className="class-subtitle">
          {props.classroom.subtitle}
        </span>
      ) : (
        ''
      )
    );
  const classroom: IClassroom = props.classroom;
  return (
    <div id="container">
      <header className="class-splash">
        <h1 className="class-name">{props.classroom.name}</h1>
        {props.classroom.subtitle ? (
          <span className="class-subtitle">
            {props.classroom.subtitle}
          </span>
        ) : (
          ''
        )}
      </header>
      <main>
        <p>f</p>
      </main>
    </div>
  );
}
