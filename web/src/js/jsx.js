// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: AGPL-3.0-or-later

const attributeMap = {
  className: 'class',
  htmlFor: 'for',
};

export function createElement(tag, props, ...children) {
  if (typeof tag === 'function') return tag(props, ...children);
  const element = document.createElement(tag);

  Object.entries(props || {}).forEach(([name, value]) => {
    if (name.startsWith('on') && name.toLowerCase() in window)
      element.addEventListener(name.toLowerCase().substr(2), value);
    else if (attributeMap.hasOwnProperty(name)) {
      element.setAttribute(attributeMap[name], value.toString());
    } else element.setAttribute(name, value.toString());
  });

  children.forEach((child) => {
    appendChild(element, child);
  });

  return element;
}

export function createFragment(props, ...children) {
  return children;
}

function appendChild(parent, child) {
  if (Array.isArray(child))
    child.forEach((nestedChild) => appendChild(parent, nestedChild));
  else
    parent.appendChild(
      child.nodeType ? child : document.createTextNode(child)
    );
}
