/**
 * Dachen Classroom
 *
 * @license
 * SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * Copyright © 2022 Dachen.
 *
 * Dachen Classroom is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * license, or (at your option) any later version.
 *
 * Dachen Classroom is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * Your warranty conditions may be different depending on your vendor or
 * hosting provider, for example an uptime guarantee.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Dachen Classroom. If not, see
 * https://www.gnu.org/licenses/.
 */

import Classroom from './views/Classroom';
import { createElement, createFragment } from './jsx';

document.getElementById('container').replaceWith(
  createElement(Classroom, {
    classroom: {
      id: '3fffc24f-db5f-420b-b8a4-801798269c98',
      name: 'Honors Algebra II',
      // subtitle: 'Subtitle',
      metadata: {
        room: '214',
        course_name: 'HAlg-II',
        splash: {
          color: '#33d17a',
        },
      },
      teachers: ['d67d22cd-b301-4ef9-a2d6-ce116676038f'],
      archived: false,
    },
  })
);
