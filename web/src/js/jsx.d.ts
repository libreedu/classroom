// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: AGPL-3.0-or-later

type Props = Record<string, any>;

export declare function createElement(
  tag: string | Function,
  props: Props,
  ...children: HTMLElement[]
): HTMLElement;
export declare function createFragment(
  props: Props,
  ...children: HTMLElement[]
): HTMLElement[];
