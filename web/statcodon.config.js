// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: AGPL-3.0-or-later

const fs = require('node:fs');
const path = require('node:path');
const { rollup } = require('rollup');
const rollupPlugins = [
  require('@rollup/plugin-typescript')({ sourceMap: true }),
  require('rollup-plugin-terser').terser({
    mangle: false,
    safari10: true,
  }),
];

const config = {
  srcDir: 'src',
  outDir: 'dist',
  templateDir: 'tmpl',
  formats: {},
  ignore: ['src/acknowledgements.yml', 'src/css/**/!(main.scss|Cantarell-VF.woff2)', 'src/js/**/!(main.ts)'],
};

config.formats.html = {
  compile: (text) => ({ text }),
  minify: (text) =>
    require('html-minifier').minify(text, {
      collapseWhitespace: true,
      removeComments: true,
      ignoreCustomComments: [/\@copyright/, /\@license/],
    }),
};

config.formats.ts = { outExt: 'js' };
config.formats.ts.compile = async (_, f) => {
  const bundle = await rollup({
    input: path.join('src', f + '.ts'),
    plugins: rollupPlugins,
  });
  const { output } = await bundle.generate({
    file: 'main.js',
    sourcemap: true,
  });
  fs.writeFileSync(
    path.join('dist', f + '.js.map'),
    JSON.stringify(output[0].map)
  );
  return {
    text: '//# sourceMappingURL=main.js.map\n' + output[0].code,
  };
};
config.formats.ts.minify = (t) => t;

config.formats.pug = {
  compile: (text, f) => {
    let template,
      templateOptions = {};
    // Pandoc metadata block
    if (text.startsWith('---')) {
      const split = text.split('---');
      if (split.length >= 3) {
        templateOptions = require('yaml').parse(split[1]);
        template = templateOptions.template;
      }
      text = split.slice(2).join('---');
    }

    return {
      text: require('pug').compile(text, {
        filename: path.join('src', f + '.pug'),
      })({require, __filename:f}),
      template: template || 'base',
      templateOptions: templateOptions || {
        seotitle: 'Dachen Classroom',
      },
    };
  },
  outExt: 'html',
  page: true,
};

const markdown = new (require('markdown-it'))({ html: true });
config.formats.md = {
  compile: (text) => {
    let template,
      templateOptions = {};
    // Pandoc metadata block
    if (text.startsWith('---')) {
      const split = text.split('---');
      if (split.length >= 3) {
        templateOptions = require('yaml').parse(split[1]);
        template = templateOptions.template;
      }
      text = split.slice(2).join('---');
    }

    return {
      text: markdown.render(text),
      template: template || 'default',
      templateOptions: templateOptions,
    };
  },
  outExt: 'html',
  page: true,
};

const sass = require('sass');
const cleanCSS = new (require('clean-css'))({});
config.formats.scss = {
  compile: (t, f) => ({
    // FIXME: sass.renderSync is deprecated
    text: sass
      .renderSync({
        data: t,
        includePaths: [path.join(config.srcDir, path.dirname(f))],
      })
      .css.toString('utf8'),
  }),
  minify: (t) => cleanCSS.minify(t).styles,
  outExt: 'css',
};

module.exports = config;
