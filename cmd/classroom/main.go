// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package main

import (
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"codeberg.org/libreedu/classroom/api"
	"codeberg.org/libreedu/classroom/db"
	"codeberg.org/libreedu/classroom/plugins"
	"codeberg.org/libreedu/common/log"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/spf13/viper"
)

var logger *log.Logger

func config() error {
	viper.SetConfigName("config")
	viper.SetConfigType("yml")
	viper.AddConfigPath(".")
	viper.SetDefault("PORT", "3000")
	viper.SetDefault("ALLOWED_ISSUERS", []string{})
	viper.SetDefault("DB_ADDR", "postgres://postgres:password@localhost:5432/postgres?sslmode=disable")
	return viper.ReadInConfig()
}

func server() {
	go db.Init(log.New(os.Stderr, "db"))

	r := chi.NewRouter()
	r.Use(middleware.AllowContentType("application/json", "application/x-www-form-urlencoded"))
	r.Use(func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("server", "Dachen Classroom")
			h.ServeHTTP(w, r)
		})
	})
	r.Group(api.Init(log.New(os.Stderr, "core api")))

	plugins.DefLoaderParams.Router = r
	plugins.DefParams.DB = db.DB
	plugins.Init(log.New(os.Stderr, "plugins"))

	logger.Info() <- "Listening on port " + viper.GetString("PORT")
	if err := http.ListenAndServe(":"+viper.GetString("PORT"), r); err != http.ErrServerClosed {
		panic(err.Error())
	}
}

func main() {
	if len(os.Args) > 1 {
		switchCmd()
		os.Exit(0)
	}

	logger = log.New(os.Stderr, "default")

	if err := config(); err != nil {
		panic(err.Error())
	}
	go server()

	ch := make(chan os.Signal, 1)
	signal.Notify(ch)
	for {
		switch <-ch {
		case os.Interrupt, syscall.SIGTERM, syscall.SIGINT:
			logger.Infof("Received interrupt signal, exiting")
			plugins.Destroy()
			return
		}
	}
}
