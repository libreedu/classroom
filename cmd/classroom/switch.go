// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package main

import (
	"context"
	"fmt"
	"os"
	"time"

	"codeberg.org/libreedu/classroom/db"
	"github.com/google/uuid"
)

func switchCmd() int8 {
	switch os.Args[1] {
	case "auth":
		fmt.Println("note! this command is likely to be removed")

		if len(os.Args) < 3 {
			fmt.Println("error: no user ID was provided!")
			fmt.Printf("usage: %s auth <user ID>\n", os.Args[0])
			return 1
		}
		userId := os.Args[2]

		if err := config(); err != nil {
			fmt.Printf("error: failed to read config: %s\n", err.Error())
		}
		if err := db.InitHeadless(); err != nil {
			fmt.Printf("error: failed to establish database connection: %s\n", err.Error())
			return 1
		}

		if authId, err := uuid.NewRandom(); err != nil {
			fmt.Printf("error: %s\n", err.Error())
			return 1
		} else if _, err := db.DB.NewInsert().Model(&db.AuthID{ID: authId.String(), UID: userId, IssuedAt: time.Now()}).Exec(context.Background()); err != nil {
			fmt.Printf("error: %s\n", err.Error())
			return 0
		} else {
			fmt.Println(authId.String())
		}
		return 0

	default:
		fmt.Printf("error: unknown command %s\n", os.Args[1])
		return 1
	}
}
