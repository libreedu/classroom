// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package plugins

import (
	"github.com/go-chi/chi/v5"
	"github.com/uptrace/bun"
)

type LoaderParams struct {
	Router chi.Router
}
type PluginParams struct {
	DB *bun.DB
}

var DefLoaderParams LoaderParams
var DefParams PluginParams
