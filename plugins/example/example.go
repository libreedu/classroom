// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package main

import (
	"net/http"

	"codeberg.org/libreedu/classroom/plugins"
	"codeberg.org/libreedu/common/log"
	"github.com/go-chi/chi/v5"
)

var logger *log.Logger

type examplePlugin struct{}

func (p examplePlugin) Init(pl *log.Logger, _ plugins.PluginParams) error {
	logger = pl
	return nil
}
func (p examplePlugin) DBusName() string { return "org.dachenedu.Classroom.ExamplePlugin" }
func (p examplePlugin) Route(r chi.Router) {
	r.Get("/api/example-plugin", func(w http.ResponseWriter, r *http.Request) { w.Write([]byte("Hello, world!")) })
}
func (p examplePlugin) Destroy() {}

var (
	Plugin examplePlugin
	_      plugins.APIPlugin = Plugin
)
