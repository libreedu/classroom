//go:build !noplugins

// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package plugins

import (
	"os"
	"plugin"

	"codeberg.org/libreedu/common/log"
	"github.com/go-chi/chi/v5"
	"github.com/spf13/viper"
)

type GetPluginFunc func() Plugin
type Plugin interface {
	Init(*log.Logger, PluginParams) error
	// Returns the D-Bus or Java–style name for the plugin
	DBusName() string
	// Function to close connections, end requests, etc.
	Destroy()
}
type APIPlugin interface {
	Plugin
	// Separate router mounted on /
	Route(chi.Router)
}

var logger *log.Logger
var plugins []*Plugin

func Init(pl *log.Logger) {
	logger = pl
	list := viper.GetStringSlice("PLUGINS")
	if list == nil {
		return
	}
	for _, file := range list {
		if plugin, err := loadPlugin(file); err != nil {
			logger.Warnf("Failed to load plugin at %s: %s", file, err.Error())
		} else {
			plugins = append(plugins, &plugin)
			logger.Infof("Loaded plugin %s at %s", plugin.DBusName(), file)
		}
	}
}

func Destroy() {
	for _, p := range plugins {
		logger.Infof("Destroying plugin %s", (*p).DBusName())
		(*p).Destroy()
	}
}

func loadPlugin(path string) (Plugin, error) {
	var po Plugin
	p, err := plugin.Open(path)
	if err != nil {
		return nil, err
	}

	if f, err := p.Lookup("Plugin"); err == nil {
		po = interface{}(f).(Plugin)
	} else {
		return nil, err
	}

	po.Init(log.New(os.Stderr, "plugins: "+po.DBusName()), DefParams)

	if po, ok := interface{}(po).(APIPlugin); ok {
		DefLoaderParams.Router.Route("/api/plugin/"+po.DBusName(), po.Route)
	}

	return po, nil
}
