//go:build noplugins

// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package plugins

import "codeberg.org/libreedu/common/log"

func Init(logger *log.Logger) {}
func Destroy()                {}
